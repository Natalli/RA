'use strict';
let $body = $('body');
$(document).ready(function () {
    mainSlider();
    fullTabs();
    newsReadMore();
    bookingShow();
    showPopup();
    gamePopupShow();
    Up();
    if ($(window).outerWidth() < 768) {
        roomsSliders('.roomSlider', '.roomSliderNav');
        mobMenu();
        slideToggle();
        gamePopupHideMobile();
    } else {
        roomsSliders('.selected .roomSlider', '.selected .roomSliderNav');
        // destroyRoomSlider();
        tabs();
        bookingHeight();
        gamePopupHide();
    }

    $(window).resize(function () {

        if ($(window).outerWidth() > 767) {
            destroyRoomSlider();
            setTimeout(tabs(), 1000);
            bookingHeight();
            gamePopupHide();
            // roomsSliders('.selected .roomSlider', '.selected .roomSliderNav');
            // tabs();
        } else {
            roomsSliders('.roomSlider', '.roomSliderNav');
            mobMenu();
            slideToggle();
            gamePopupHideMobile();
        }
    })
});

function mainSlider() {
    $('.mainSlider').slick({
        dots: false,
        arrows: true,
        infinite: true,
        slidesToShow: 1,
        prevArrow: '<div class="arrow arrow-left"><svg class="arrowIcon" viewBox="0 0 28 80" xmlns="http://www.w3.org/2000/svg"><g><path class="st8" fill="var(--menu-icon-path-fill)" d="M0,40L0,40L0,40L21.9,0h6L28,0.1L6.2,40L28,79.9L27.9,80h-6L0,40L0,40L0,40z"/></g></svg></div>',
        nextArrow: '<div class="arrow arrow-right"><svg class="arrowIcon" viewBox="0 0 28 80" xmlns="http://www.w3.org/2000/svg"><g><path class="st8" fill="var(--menu-icon-path-fill)" d="M0,40L0,40L0,40L21.9,0h6L28,0.1L6.2,40L28,79.9L27.9,80h-6L0,40L0,40L0,40z"/></g></svg></div>',
        speed: 500,
        cssEase: 'linear'
    });
}

function mobMenu() {
    $('.nav-trigger').on('click', function () {
        if ($('body').hasClass('show-nav')) {
            $('body').removeClass('show-nav');
        }
        else {
            $('body').addClass('show-nav');
        }
    });
}

function tabs() {
    $('.tabs-container .wrap-container').each(function () {
        let $thisContent = $(this);

        $('.tabs-nav li').on('click', function () {
            let $thisButton = $(this);
            if ($thisButton.data('tab') === $thisContent.data('tab')) {
                $('.tabs-nav li').removeClass('active');
                $('.tabs-container .wrap-container').removeClass('selected');
                $thisContent.addClass('selected');
                console.log('tab', 12);
                roomsSliders('.selected .roomSlider', '.selected .roomSliderNav');
                $thisButton.addClass('active');
            }
        });
    });
}

function fullTabs() {
    $('.full-tab .tabs-container .wrap-container').each(function () {
        let $thisContent = $(this);
        $('.full-tab .tabs-nav li').on('click', function () {
            let $thisButton = $(this);
            if ($thisButton.data('tab') === $thisContent.data('tab')) {
                $('.full-tab .tabs-nav li').removeClass('active');
                $('.full-tab .tabs-container .wrap-container').removeClass('selected');
                $thisContent.addClass('selected');
                $thisButton.addClass('active');
            }
        });
    });
}

function destroyRoomSlider() {
    setTimeout(fDestroy, 500);

    // if($('.roomSlider').hasClass('slick-initialized')) {
    //     $('.roomSlider').slick('unslick');
    //     console.log('uns', 1);
    // }
    // if($('.roomSliderNav').not('slick-initialized')) {
    //     $('.roomSliderNav').slick('unslick');
    //     console.log('uns', 2);
    // }

}

function fDestroy() {
    $('.tabs-container .wrap-container').each(function () {
        $(this).find('.roomSlider').slick('unslick');
        $(this).find('.roomSliderNav').slick('unslick');
        console.log('uns', 3);
    });
}

function roomsSliders(slider, sliderNav) {
    $(slider).not('.slick-initialized').slick({
        speed: 500,
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        arrows: true,
        prevArrow: '<div class="arrow arrow-left"><svg class="arrowIcon" viewBox="0 0 28 80" xmlns="http://www.w3.org/2000/svg"><g><path class="st8" fill="var(--menu-icon-path-fill)" d="M0,40L0,40L0,40L21.9,0h6L28,0.1L6.2,40L28,79.9L27.9,80h-6L0,40L0,40L0,40z"/></g></svg></div>',
        nextArrow: '<div class="arrow arrow-right"><svg class="arrowIcon" viewBox="0 0 28 80" xmlns="http://www.w3.org/2000/svg"><g><path class="st8" fill="var(--menu-icon-path-fill)" d="M0,40L0,40L0,40L21.9,0h6L28,0.1L6.2,40L28,79.9L27.9,80h-6L0,40L0,40L0,40z"/></g></svg></div>',
        asNavFor: '.roomSliderNav',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false
                }
            }
        ]
    });
    $(sliderNav).not('.slick-initialized').slick({
        speed: 500,
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        focusOnSelect: true,
        arrows: true,
        prevArrow: '<div class="arrow arrow-left"><svg class="arrowIcon" viewBox="0 0 28 80" xmlns="http://www.w3.org/2000/svg"><g><path class="st8" fill="var(--menu-icon-path-fill)" d="M0,40L0,40L0,40L21.9,0h6L28,0.1L6.2,40L28,79.9L27.9,80h-6L0,40L0,40L0,40z"/></g></svg></div>',
        nextArrow: '<div class="arrow arrow-right"><svg class="arrowIcon" viewBox="0 0 28 80" xmlns="http://www.w3.org/2000/svg"><g><path class="st8" fill="var(--menu-icon-path-fill)" d="M0,40L0,40L0,40L21.9,0h6L28,0.1L6.2,40L28,79.9L27.9,80h-6L0,40L0,40L0,40z"/></g></svg></div>',
        asNavFor: '.roomSlider',
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 4
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2
                }
            }
        ]
    });
}

function slideToggle() {
    $('.filters .filter-sub-ttl').on('click', function () {
        $(this).next('.filter-items').slideToggle();
    })
}

function newsReadMore() {
    $('.newsWrapper .readMore').on('click', function () {
        $(this).closest('.item').toggleClass('show')
    });
}

function bookingShow() {


    if ((window).outerWidth > 767) {
        $('.bookingBtn').on('click', function () {
            $(this).closest('.bookingBox').toggleClass('show');
            $body.toggleClass('overlayShow');
        });
    } else {

        $('.bookingMobileBtn').on('click', function () {
            $body.addClass('overlayShow');
            $(this).closest('body').find('.bookingBox').addClass('show')

            $('.bookingBox .close').on('click', function () {
                $(this).closest('.bookingBox').removeClass('show');
                $body.removeClass('overlayShow');
            })
        })
    }
}

function bookingHeight() {
    let heightWindow = $(window).outerHeight(),
        bookingHeight = $('.bookingForm').outerHeight(),
        maxHeight = heightWindow - 150;

    if (bookingHeight < maxHeight) {
        $('.bookingBox').css('height', 'auto')

    } else {
        $('.bookingBox').css('height', maxHeight + 'px')
    }
}

function showPopup() {
    let popup = $('#popupSuccess');
    let $body = $('body');
    $('#onBooking').on('click', function () {
        popup.show();
        // $body.addClass('overlayShow');
    });

    $('#btnSuccess').on('click', function () {
        popup.hide();
        // $body.removeClass('overlayShow');
    })
}

function Up() {
    let upBox = $('.upBox');
    $(window).on('scroll', function () {
        let windowTop = $(window).scrollTop();
        if (windowTop > 0) {
            upBox.show();
        } else {
            upBox.hide();
        }
    });

    upBox.on('click', function () {
        $('body,html').animate({scrollTop: 0}, 1500);
    });
}

function scrollBox() {
    if ($(document).find('.scrollBox')) {
        $('.scrollBox').scrollbar();
    }
}

function gamePopupShow() {
    let item = $('.games .item'),
        popup = $('.gamePopup');

    item.on('click', function (e) {
        e.preventDefault();
        let pathImg = $(this).find('.href').attr('href'),
            popupContent = $(this).find('.popup-content').html(),
            boxImg = popup.find('.game-img'),
            boxContent = $('.gamePopup .game-content .scrollBox .scrollContainer');

        boxImg.append(
            $('<img/>').attr('src', pathImg)
        );
        boxContent.append(popupContent);
        scrollBox();
        popup.show();
        $body.toggleClass('overlayShow');
    });
}

function gamePopupHide() {
    let popup = $('.gamePopup');
    $('.overlay').on('click', function () {
        popup.hide();
        $('.gamePopup .game-img').empty();
        $('.gamePopup .game-content .scrollBox .scrollContainer').empty();
        $body.removeClass('overlayShow');
    });
}
function gamePopupHideMobile() {
    let popup = $('.gamePopup');
    $('.gamePopup .close').on('click', function () {
        popup.hide();
        $('.gamePopup .game-img').empty();
        $('.gamePopup .game-content .scrollBox .scrollContainer').empty();
        $body.removeClass('overlayShow');
    })
}